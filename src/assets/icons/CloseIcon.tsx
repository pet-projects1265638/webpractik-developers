import React from 'react';

interface Props {
  width: string;
  height: string;
  fill: string;
  stroke: string;
}

function CloseIcon({
  stroke = '#fff',
  fill = 'none',
  width = '2rem',
  height = '2rem',
}: Partial<Props>) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 32 32"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title />
      <g id="cross">
        <line
          style={{
            stroke,
            fill,
            strokeLinecap: 'round',
            strokeLinejoin: 'round',
            strokeWidth: '2px',
          }}
          className="cls-1"
          x1="7"
          x2="25"
          y1="7"
          y2="25"
        />
        <line
          style={{
            stroke,
            fill,
            strokeLinecap: 'round',
            strokeLinejoin: 'round',
            strokeWidth: '2px',
          }}
          className="cls-1"
          x1="7"
          x2="25"
          y1="25"
          y2="7"
        />
      </g>
    </svg>
  );
}

export default CloseIcon;
