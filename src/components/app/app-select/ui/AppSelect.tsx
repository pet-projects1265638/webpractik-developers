import { ChangeEvent } from 'react';
import '../styles/appSelect.scss';

interface Props {
  value: string;
  errors?: string[];
  onChange: (element: ChangeEvent<HTMLSelectElement>) => void;
  items: Array<{ label: string; value: string }>;
}

function AppSelect({
  errors, items, onChange, value,
}: Partial<Props>) {
  return (
    <div className="app-select">
      <select className="app-select__field" value={value} onChange={onChange}>
        {items?.map((item) => (
          <option key={item.value} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
      {errors?.length !== 0 && (
        <div className="app-select__errors">
          {errors?.map((err) => <span>{err}</span>)}
        </div>
      )}
    </div>
  );
}

export default AppSelect;
