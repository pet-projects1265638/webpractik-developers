import { ChangeEvent, forwardRef } from 'react';
import '../styles/appInput.scss';

type AppInputProps = {
  placeholder?: string;
  value?: string;
  type?: 'text' | 'number';
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  errors?: Array<string>;
};
const AppInput = forwardRef<HTMLInputElement, AppInputProps>(
  (
    // eslint-disable-next-line object-curly-newline
    { onChange, errors = [], type = 'text', value = '', placeholder = '' },
    ref,
  ) => (
    <div className="app-input">
      <input
        ref={ref}
        className="app-input__field"
        aria-label="input-field"
        value={value}
        placeholder={placeholder}
        type={type}
        onChange={onChange}
      />
      {errors.length !== 0 && (
        <div className="app-input__errors-wrapper">
          {errors.map((error) => (
            <span key={error}>{error}</span>
          ))}
        </div>
      )}
    </div>
  ),
);

export default AppInput;
