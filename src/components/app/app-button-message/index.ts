import AppButtonMessage from './ui/AppButtonMessage';

export type { IButtonMessage } from './ui/AppButtonMessage';

export default AppButtonMessage;
