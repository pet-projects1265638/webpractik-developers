import { MouseEvent } from 'react';

export interface IButtonMessage {
  // eslint-disable-next-line react/require-default-props
  disabled?: boolean;
  // eslint-disable-next-line react/require-default-props
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  onMessage: (val: string) => void;
}

function AppButtonMessage({
  onMessage,
  disabled = false,
  onClick,
}: IButtonMessage) {
  const clickHandle = (e: MouseEvent<HTMLButtonElement>) => {
    onClick?.(e);
    onMessage?.('Это тестовое сообщение, не пугайтесь');
  };

  return (
    <button type="button" disabled={disabled} onClick={clickHandle}>
      Call message
    </button>
  );
}

export default AppButtonMessage;
