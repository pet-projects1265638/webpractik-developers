import React from 'react';
import { Typography } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import VisibilityIcon from '@mui/icons-material/Visibility';
import MessageIcon from '@mui/icons-material/Message';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import { IVideo } from '../../../shared/types/video';

import './styles/videoCard.scss';

interface Props {
  video: IVideo;
}

function VideoCard({ video }: Props) {
  return (
    <div className="video-card">
      <div>
        <img
          src={
            video.snippet.thumbnails.maxres?.url ||
            video.snippet.thumbnails.default?.url
          }
          alt=""
        />
      </div>
      <div className="video-card__content">
        <Typography
          className="video-card__content-title"
          margin=".54rem 0"
          variant="h3"
          fontSize="1.24rem"
        >
          {video.snippet.title}
        </Typography>
        <div className="video-card__content-author">
          <AccountCircleIcon
            sx={{
              width: '2rem',
              height: '2rem',
              color: '#fff',
              marginRight: '8px',
            }}
          />
          <span>{video.snippet.channelTitle}</span>
        </div>
        <div className="video-card__content-stat">
          <VisibilityIcon sx={{ color: '#fff', width: '1.5rem' }} />
          <span className="video-card__content-stat-text">
            {video.statistics.viewCount}
          </span>
          <ThumbUpIcon sx={{ color: '#fff', width: '1.5rem' }} />
          <span className="video-card__content-stat-text">
            {video.statistics.likeCount}
          </span>
          <MessageIcon sx={{ color: '#fff', width: '1.5rem' }} />
          <span className="video-card__content-stat-text">
            {video.statistics.commentCount}
          </span>
        </div>
      </div>
    </div>
  );
}

export default VideoCard;
