import { useContext } from 'react';
import { IDeveloper } from '../../../../shared/types/developer';
import ProfileIcon from '../../../../assets/icons/ProfileIcon';
import '../styles/developerCard.scss';
import CloseIcon from '../../../../assets/icons/CloseIcon';
import { DevelopersContext } from '../../../../shared/constants/developers';

type Props = {
  developer: IDeveloper;
  developerIndex: number;
};

function DeveloperCard({ developer, developerIndex }: Props) {
  const { deleteDeveloper } = useContext(DevelopersContext);
  const deleteDeveloperHandle = () => {
    deleteDeveloper(developerIndex);
  };
  return (
    <div className="developer-card">
      <div className="developer-card__actions">
        <button
          aria-label="delete card"
          type="button"
          className="developer-card__close-btn"
          onClick={deleteDeveloperHandle}
        >
          <CloseIcon width="1.45rem" height="1.45rem" />
        </button>
      </div>
      <div className="developer-card__icon">
        <ProfileIcon fill="#fff" width="42px" />
      </div>
      <h3 className="developer-card__title">{developer.fullName}</h3>
      <div>
        <p className="developer-card__speciality">
          Специализация: {developer.speciality}
        </p>
        <p className="developer-card__level">
          Уровень:
          {developer.level}
        </p>
      </div>
    </div>
  );
}

export default DeveloperCard;
