import { Controller, useForm } from 'react-hook-form';
import useOverlay from '../../../../shared/hooks/useOverlay';
import AppInput from '../../../app/app-input';
import AppSelect from '../../../app/app-select';
import {
  selectLevelItems,
  selectSpecialityItems,
} from '../../../../shared/constants/developers';
import { IDeveloper } from '../../../../shared/types/developer';
import '../styles/developerFormCreate.scss';

interface Props {
  onChangeOpen: (val: boolean) => void;
  onSubmit: (data: IDeveloper) => void;
}

function DeveloperFormCreate({ onChangeOpen, onSubmit }: Props) {
  const { control, handleSubmit } = useForm<IDeveloper>({
    defaultValues: {
      fullName: '',
      level: 'Junior',
      speciality: 'Frontend',
    },
  });
  const submitHandle = handleSubmit((data) => {
    onSubmit(data);
  });

  const { outsideClickHandle, overlayElementRef } = useOverlay<HTMLDivElement>(
    () => onChangeOpen(false),
  );

  const closeHandle = () => {
    onChangeOpen(false);
  };

  return (
    <div
      role="presentation"
      ref={overlayElementRef}
      className="overlay-wrapper"
      onClick={outsideClickHandle}
    >
      <div className="overlay-content">
        <form className="developer-form" onSubmit={submitHandle}>
          <h1 className="developer-form__title">Create new developer</h1>
          <Controller
            control={control}
            name="fullName"
            render={({ field: { onChange, value } }) => (
              <AppInput value={value} onChange={onChange} />
            )}
          />
          <Controller
            control={control}
            name="level"
            render={({ field: { onChange, value } }) => (
              <AppSelect
                value={value}
                onChange={onChange}
                items={selectLevelItems}
              />
            )}
          />
          <Controller
            control={control}
            name="speciality"
            render={({ field: { onChange, value } }) => (
              <AppSelect
                value={value}
                onChange={onChange}
                items={selectSpecialityItems}
              />
            )}
          />
          <div className="developer-form__actions">
            <button
              type="button"
              className="developer-form__close-btn"
              onClick={closeHandle}
            >
              Close
            </button>
            <button type="submit" className="developer-form__submit-btn">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default DeveloperFormCreate;
