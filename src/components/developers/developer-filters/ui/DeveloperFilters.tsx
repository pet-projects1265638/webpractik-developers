import { ChangeEvent, useContext } from 'react';
import AppSelect from '../../../app/app-select';
import AppInput from '../../../app/app-input';
import {
  DevelopersContext,
  selectLevelItems,
  selectSpecialityItems,
} from '../../../../shared/constants/developers';

function DeveloperFilters() {
  const { filters, setDeveloperFilter } = useContext(DevelopersContext);
  const filterFullNameHandle = (element: ChangeEvent<HTMLInputElement>) => {
    setDeveloperFilter('fullName', element.target.value);
  };
  const filterLevelHandle = (element: ChangeEvent<HTMLSelectElement>) => {
    setDeveloperFilter('level', element.target.value);
  };
  const filterSpecialityHandle = (element: ChangeEvent<HTMLSelectElement>) => {
    setDeveloperFilter('speciality', element.target.value);
  };

  return (
    <>
      <AppInput
        value={filters.fullName}
        placeholder="Find by name"
        onChange={filterFullNameHandle}
      />
      <AppSelect
        value={filters.level}
        onChange={filterLevelHandle}
        items={selectLevelItems}
      />
      <AppSelect
        value={filters.speciality}
        onChange={filterSpecialityHandle}
        items={selectSpecialityItems}
      />
    </>
  );
}

export default DeveloperFilters;
