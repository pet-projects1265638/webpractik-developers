import { configureStore } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import type { TypedUseSelectorHook } from 'react-redux';
import { videosApi } from './videos/videosApi';
import videosReducer from './videos/videosSlice';

export const store = configureStore({
  reducer: {
    [videosApi.reducerPath]: videosApi.reducer,
    videos: videosReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([videosApi.middleware]),
});

export { videosReducer };

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
