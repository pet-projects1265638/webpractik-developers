import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { VideoResponse } from '../../../shared/types/video';

const YOUTUBE_KEY = 'AIzaSyCoSAVqiUQB2EELjS04F0ZLorTBXx-XQNg';

const DEFAULT_URL = 'https://www.googleapis.com/youtube/v3';

export const videosApi = createApi({
  reducerPath: 'videosApi',
  baseQuery: fetchBaseQuery({ baseUrl: DEFAULT_URL }),
  endpoints: (builder) => ({
    // prettier-ignore
    getPopularVideosByRegion: builder.query<
    VideoResponse,
    { pageToken?: string; regionCode: string }
    >({
      query: ({ regionCode, pageToken = '' }) =>
        `/videos?key=${YOUTUBE_KEY}&part=snippet,statistics&maxResults=20&chart=mostPopular&regionCode=${regionCode}${
          pageToken ? `&pageToken=${pageToken}` : ''
        }`,
    }),
  }),
});
