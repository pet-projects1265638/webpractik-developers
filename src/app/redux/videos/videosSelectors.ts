import { RootState } from '../store';

export const selectVideosRegionCode = (state: RootState) =>
  state.videos.regionCode;
export const selectVideosList = (state: RootState) => state.videos.list;
export const selectVideosPageToken = (state: RootState) =>
  state.videos.nextPageToken;
