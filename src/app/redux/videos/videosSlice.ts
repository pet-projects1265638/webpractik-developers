import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IVideo } from '../../../shared/types/video';

interface VideosState {
  regionCode: string;
  list: IVideo[];
  nextPageToken: string;
}

const initialState: VideosState = {
  regionCode: 'RU',
  list: [],
  nextPageToken: '',
};

export const videosSlice = createSlice({
  name: 'videos',
  initialState,
  reducers: {
    setRegionCode(state, action: PayloadAction<string>) {
      // eslint-disable-next-line no-param-reassign
      state.regionCode = action.payload;
    },
    addVideos(state, action: PayloadAction<IVideo[]>) {
      // eslint-disable-next-line no-param-reassign
      state.list = state.list.concat(action.payload);
    },
    setVideos(state, action: PayloadAction<IVideo[]>) {
      // eslint-disable-next-line no-param-reassign
      state.list = action.payload;
    },
    setPageToken(state, action: PayloadAction<string>) {
      // eslint-disable-next-line no-param-reassign
      state.nextPageToken = action.payload;
    },
  },
});

// prettier-ignore
export const {
  setRegionCode, addVideos, setVideos, setPageToken,
} = videosSlice.actions;

export default videosSlice.reducer;
