import React from 'react';
import { Outlet } from 'react-router-dom';
import HeaderWidget from '../../widgets/header-widget';

function DefaultRouter() {
  return (
    <>
      <HeaderWidget />
      <Outlet />
    </>
  );
}

export default DefaultRouter;
