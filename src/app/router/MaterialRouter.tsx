import React from 'react';
import { Outlet } from 'react-router-dom';
import MaterialHeaderWidget from '../../widgets/material-header-widget';

function MaterialRouter() {
  return (
    <>
      <MaterialHeaderWidget />
      <main className="material-router__wrapper">
        <Outlet />
      </main>
    </>
  );
}

export default MaterialRouter;
