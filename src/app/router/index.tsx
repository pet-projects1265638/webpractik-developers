import { Route, Routes } from 'react-router-dom';
import { lazy, Suspense } from 'react';
import './styles/routers.scss';
import DefaultRouter from './DefaultRouter';
import MaterialRouter from './MaterialRouter';

const MainView = lazy(() => import('../../views/main-view'));
const ValidationsView = lazy(() => import('../../views/validations-view'));
const DevelopersView = lazy(() => import('../../views/developers-view'));
const FetchDataView = lazy(() => import('../../views/fetch-data-view'));
const MaterialTableView = lazy(() => import('../../views/material-table-view'));
const MaterialFormView = lazy(() => import('../../views/material-form-view'));
const VideosView = lazy(() => import('../../views/videos-view'));

function Router() {
  return (
    <Routes>
      <Route path="/" element={<DefaultRouter />}>
        <Route
          path="/"
          element={
            <Suspense fallback="sorry">
              <MainView />
            </Suspense>
          }
        />
        <Route
          path="/validations"
          element={
            <Suspense fallback="sorry">
              <ValidationsView />
            </Suspense>
          }
        />
        <Route
          path="/developers"
          element={
            <Suspense fallback="sorry">
              <DevelopersView />
            </Suspense>
          }
        />
        <Route
          path="/fetch-data"
          element={
            <Suspense fallback="sorry">
              <FetchDataView />
            </Suspense>
          }
        />
        <Route
          path="/videos"
          element={
            <Suspense>
              <VideosView />
            </Suspense>
          }
        />
      </Route>
      <Route path="/material" element={<MaterialRouter />}>
        <Route
          path="table"
          element={
            <Suspense>
              <MaterialTableView />
            </Suspense>
          }
        />
        <Route
          path="form"
          element={
            <Suspense>
              <MaterialFormView />
            </Suspense>
          }
        />
      </Route>
    </Routes>
  );
}

export default Router;
