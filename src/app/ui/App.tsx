import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import ru from 'date-fns/locale/ru';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers';
import NotificationsWidget from '../../widgets/notifications-widget';
import Router from '../router';
import { store } from '../redux/store';

import '../styles/App.scss';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ru}>
          <NotificationsWidget>
            <BrowserRouter>
              <Router />
            </BrowserRouter>
          </NotificationsWidget>
        </LocalizationProvider>
      </Provider>
    </div>
  );
}

export default App;
