export type DeveloperLevels = 'Junior' | 'Middle' | 'Senior';
export type DeveloperSpecialities = 'Frontend' | 'Backend' | 'DevOps';

export interface IDeveloper {
  fullName: string;
  level: DeveloperLevels;
  speciality: DeveloperSpecialities;
}

export interface IDeveloperFilters {
  fullName: string;
  level: DeveloperLevels | '';
  speciality: DeveloperSpecialities | '';
}
