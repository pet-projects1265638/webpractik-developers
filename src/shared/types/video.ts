export interface IVideo {
  etag: string;
  id: string;
  kind: string;
  snippet: {
    categoryId: string;
    channelId: string;
    channelTitle: string;
    description: string;
    liveBroadcastContent: string;
    publishedAt: string;
    title: string;
    localized: {
      title: string;
      description: string;
    };
    thumbnails: {
      default: {
        width: number;
        height: number;
        url: string;
      };
      medium: {
        width: number;
        height: number;
        url: string;
      };
      high: {
        width: number;
        height: number;
        url: string;
      };
      maxres: {
        width: number;
        height: number;
        url: string;
      };
      standard: {
        width: number;
        height: number;
        url: string;
      };
    };
  };
  statistics: {
    commentCount: string;
    favoriteCount: string;
    likeCount: string;
    viewCount: string;
  };
}

export interface VideoResponse {
  etag: string;
  kind: string;
  nextPageToken: string;
  prevPageToken?: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  };
  items: Array<IVideo>;
}
