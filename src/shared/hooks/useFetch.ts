import { useCallback, useEffect, useState } from 'react';

interface IFetch {
  url: string;
  method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
  body?: unknown;
}

export default function useFetch<T>({ url, method = 'GET', body }: IFetch) {
  const [data, setData] = useState<T | null>(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const request = useCallback(async () => {
    setLoading(true);
    setData(null);
    setError('');
    try {
      const requestResponse = await fetch(url, {
        method,
        body: body ? JSON.stringify(body) : null,
        headers: { 'Content-Type': 'application/json' },
      });
      if (!requestResponse.ok) {
        setError(
          `Failed to get what I want, got status: ${requestResponse.status}`,
        );
        return;
      }
      setData(await requestResponse.json());
    } catch (err: unknown) {
      console.log(err);
    } finally {
      setLoading(false);
    }
  }, [body, method, url]);

  useEffect(() => {
    request();
  }, [request]);

  return {
    data,
    loading,
    error,
  };
}
