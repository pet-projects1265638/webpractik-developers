import { useRef, useState } from 'react';

interface IObserverConfig {
  triggerOnce?: boolean;
  threshold: number;
  rootMargin?: string;
  root?: Element | Document | null;
}

export function useObserver({
  threshold,
  root,
  rootMargin = '',
  triggerOnce = false,
}: IObserverConfig) {
  const [inView, setInView] = useState(false);
  const observerHandle = (
    [entry]: IntersectionObserverEntry[],
    observer: IntersectionObserver,
  ) => {
    if (entry.isIntersecting) {
      if (triggerOnce) observer.unobserve(entry.target);
    }
    setInView(entry.isIntersecting);
  };
  const observerRef = useRef(
    new IntersectionObserver(observerHandle, {
      threshold,
      root,
      rootMargin,
    }),
  );

  const observe = (target: HTMLElement) => {
    observerRef.current.observe(target);
  };
  const unobserve = () => {
    observerRef.current.disconnect();
  };
  return { observe, unobserve, inView };
}
