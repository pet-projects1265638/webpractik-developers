import { useEffect, useRef, MouseEvent } from 'react';

export default function useOverlay<T extends HTMLElement>(
  callback: () => void,
) {
  const overlayElementRef = useRef<T>(null);

  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'auto';
    };
  }, []);
  const outsideClickHandle = (event: MouseEvent<T>) => {
    if (event.target === overlayElementRef.current) {
      callback();
    }
  };

  return {
    outsideClickHandle,
    overlayElementRef,
  };
}
