import { useEffect, useState } from 'react';

export default function useResize() {
  const [currentWindowSize, setCurrentWindowSize] = useState<number>(
    window.screen.width,
  );

  useEffect(() => {
    const resizeHandle = (e: UIEvent) => {
      setCurrentWindowSize(e.view?.window.screen.width || window.screen.width);
    };

    window.addEventListener('resize', resizeHandle);

    return () => {
      window.removeEventListener('resize', resizeHandle);
    };
  }, []);

  return { currentWindowSize };
}
