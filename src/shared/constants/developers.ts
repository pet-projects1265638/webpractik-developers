import { createContext } from 'react';
import {
  DeveloperLevels,
  DeveloperSpecialities,
  IDeveloper,
  IDeveloperFilters,
} from '../types/developer';

export interface IDevelopersContext {
  developersList: IDeveloper[];
  filters: IDeveloperFilters;
  createDeveloper: (val: IDeveloper) => void;
  deleteDeveloper: (index: number) => void;
  setDeveloperFilter: (
    filterName: keyof IDeveloperFilters,
    filterValue: IDeveloperFilters[keyof IDeveloperFilters],
  ) => void;
}

export const DevelopersContext = createContext<IDevelopersContext>(
  {} as IDevelopersContext,
);

export const selectLevelItems: Array<{
  value: DeveloperLevels | '';
  label: string;
}> = [
  { value: '', label: 'Clear' },
  { value: 'Junior', label: 'Junior' },
  { value: 'Middle', label: 'Middle' },
  { value: 'Senior', label: 'Senior' },
];
export const selectSpecialityItems: Array<{
  value: DeveloperSpecialities | '';
  label: string;
}> = [
  { value: '', label: 'Clear' },
  { value: 'Frontend', label: 'Frontend' },
  { value: 'Backend', label: 'Backend' },
  { value: 'DevOps', label: 'DevOps' },
];
