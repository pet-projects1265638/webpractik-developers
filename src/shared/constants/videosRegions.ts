export const regions = [
  { label: 'Россия', value: 'RU' },
  { label: 'США', value: 'US' },
  { label: 'Франция', value: 'FR' },
  { label: 'Казахстан', value: 'KZ' },
  { label: 'Уганда', value: 'UG' },
];
