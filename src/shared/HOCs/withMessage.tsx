import { ComponentType, useContext } from 'react';
import { NotificationsContext } from '../../widgets/notifications-widget';

type IMessage = {
  onMessage: (val: string) => void;
};

export default function withMessage<T extends IMessage>(
  Component: ComponentType<IMessage>,
) {
  return function (props: Omit<T, keyof IMessage>) {
    const { createNotification } = useContext(NotificationsContext);

    return <Component {...props} onMessage={createNotification} />;
  };
}
