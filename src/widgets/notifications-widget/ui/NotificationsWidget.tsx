import { createContext, ReactNode, useMemo, useState } from 'react';
import '../styles/NotificationsWidget.scss';

export type INotification = {
  number: number;
  text: string;
};

export type INotificationsContext = {
  list: INotification[];
  deleteNotification: (val: number) => void;
  createNotification: (text: string) => void;
};

export const NotificationsContext = createContext({} as INotificationsContext);

interface Props {
  children: ReactNode;
}

function NotificationsWidget({ children }: Props) {
  const [list, setList] = useState<INotification[]>([]);

  const deleteNotification = (number: number) => {
    setList((notifications) =>
      notifications.filter((notification) => notification.number !== number),
    );
  };

  const createNotification = (text: string) => {
    setTimeout(() => deleteNotification(list.length), 3000);
    setList((notifications) => [
      ...notifications,
      {
        text,
        number: notifications.length,
      },
    ]);
  };

  const context = useMemo(
    () => ({ list, createNotification, deleteNotification }),
    [],
  );

  return (
    <NotificationsContext.Provider value={context}>
      {children}
      <div className="widget-notifications">
        {list.map((notification) => (
          <div className="widget-notification" key={notification.number}>
            {notification.text}
          </div>
        ))}
      </div>
    </NotificationsContext.Provider>
  );
}

export default NotificationsWidget;
