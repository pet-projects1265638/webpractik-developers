import NotificationsWidget from './ui/NotificationsWidget';

export type {
  INotification,
  INotificationsContext,
} from './ui/NotificationsWidget';

export { NotificationsContext } from './ui/NotificationsWidget';
export default NotificationsWidget;
