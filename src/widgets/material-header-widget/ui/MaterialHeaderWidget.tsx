import React from 'react';
import {
  AppBar,
  Avatar,
  Box,
  Toolbar,
  Typography,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import TableChartIcon from '@mui/icons-material/TableChart';
import { Link } from 'react-router-dom';
import EmailIcon from '@mui/icons-material/Email';

import '../styles/materialHeaderWidget.scss';

function MaterialHeaderWidget() {
  const menuList = [
    { label: 'Table', icon: <TableChartIcon />, link: '/material/table' },
    { label: 'Form', icon: <EmailIcon />, link: '/material/form' },
  ];
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Avatar sx={{ marginRight: '1rem' }} />
        <Typography>Ilon Mask</Typography>
      </Toolbar>
      <Box component="nav">
        <Drawer variant="permanent" className="material-drawer" open>
          <List>
            {menuList.map((item) => (
              <ListItem key={item.label} disablePadding>
                <Link style={{ width: '100%' }} to={item.link}>
                  <ListItemButton>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.label} />
                  </ListItemButton>
                </Link>
              </ListItem>
            ))}
          </List>
        </Drawer>
      </Box>
    </AppBar>
  );
}

export default MaterialHeaderWidget;
