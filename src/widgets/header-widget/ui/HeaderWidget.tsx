import { Link } from 'react-router-dom';
import '../styles/headerWidget.scss';

function HeaderWidget() {
  const routes = [
    { path: '/validations', label: 'Zod + react hook form' },
    { path: '/developers', label: 'Developers' },
    { path: '/fetch-data', label: 'Fake api fetch data' },
    { path: '/material', label: 'Material' },
    { path: '/videos', label: 'Youtube' },
  ];
  return (
    <header className="header">
      <h2 className="header-title">My best developer&apos;s project</h2>
      <nav className="header-nav">
        <ul className="header-nav__list">
          {routes.map((route) => (
            <li key={route.path} className="header-nav__list-item">
              <Link to={route.path}>{route.label}</Link>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
}

export default HeaderWidget;
