import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { Controller, useForm } from 'react-hook-form';
import AppInput from '../../../components/app/app-input';
import '../styles/validationsView.scss';
import { schema } from '../validations/schema';

function ValidationsView() {
  const {
    control,
    handleSubmit,
    formState: { errors },
    register,
    reset,
  } = useForm<z.infer<typeof schema>>({ resolver: zodResolver(schema) });
  const onSubmit = handleSubmit(() => {
    reset();
  });

  return (
    <section className="validations">
      <h1 className="validations-title">Change account settings</h1>
      <div className="validations__common-errors">{errors.common?.message}</div>
      <form className="validations-form" onSubmit={onSubmit}>
        <Controller
          control={control}
          name="oldPassword"
          render={({ field: { onChange, value } }) => (
            <AppInput
              value={value}
              placeholder="Old password"
              errors={
                errors.oldPassword?.message ? [errors.oldPassword.message] : []
              }
              onChange={onChange}
            />
          )}
        />
        <Controller
          control={control}
          name="newPassword"
          render={({ field: { onChange, value } }) => (
            <AppInput
              value={value}
              placeholder="New password"
              errors={
                errors.newPassword?.message ? [errors.newPassword.message] : []
              }
              onChange={onChange}
            />
          )}
        />
        <Controller
          control={control}
          name="confirmNewPassword"
          render={({ field: { onChange, value } }) => (
            <AppInput
              value={value}
              placeholder="Confirm new password"
              errors={
                errors.confirmNewPassword?.message
                  ? [errors.confirmNewPassword.message]
                  : []
              }
              onChange={onChange}
            />
          )}
        />
        <div className="validations-form__item">
          <div>
            <input {...register('checkAll')} type="checkbox" />
            <span>Согласен на все подписки*</span>
          </div>
          <div>{errors.checkAll?.message}</div>
        </div>
        <div>
          <select {...register('role')}>
            <option value="участник">участник</option>
            <option value="сми">сми</option>
            <option value="выступающий">выступающий</option>
          </select>
          <div>{errors.role?.message}</div>
        </div>
        <div>
          <select {...register('profession')}>
            <option value="дизайнер">дизайнер</option>
            <option value="счетовод">счетовод</option>
            <option value="менеджер">менеджер</option>
          </select>
          <div>{errors.profession?.message}</div>
        </div>
        <div className="validations-form__actions">
          <button type="submit" className="validations-form__submit">
            Submit
          </button>
          <button
            type="button"
            className="validations-form__submit"
            onClick={() => reset()}
          >
            Clear
          </button>
        </div>
      </form>
    </section>
  );
}

export default ValidationsView;
