import { z } from 'zod';

export const schema = z
  .object({
    common: z.string().optional(),
    oldPassword: z
      .string()
      .min(8, 'Password must contain at least 8 characters')
      .regex(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
        'Password must contain one uppercase letter and one number letter',
      )
      .optional(),
    newPassword: z
      .string()
      .min(8, 'Password must contain at least 8 characters')
      .regex(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
        'Password must contain one uppercase letter and one number letter',
      )
      .optional(),
    confirmNewPassword: z
      .string()
      .min(8, 'Password must contain at least 8 characters')
      .regex(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
        'Password must contain one uppercase letter and one number letter',
      )
      .optional(),
    role: z.union([
      z.literal('участник'),
      z.literal('сми'),
      z.literal('выступающий'),
    ]),
    checkAll: z.boolean().refine((val) => val, 'Required field'),
    profession: z.union([
      z.literal('дизайнер'),
      z.literal('счетовод'),
      z.literal('менеджер'),
    ]),
  })
  .refine(
    ({ role, profession }) => role !== 'сми' && profession !== 'менеджер',
    {
      path: ['profession'],
      message: 'The role of "Сми" cannot be the profession of "Менеджер"',
    },
  )
  .refine(
    ({ confirmNewPassword, newPassword }) => newPassword === confirmNewPassword,
    { path: ['common'], message: 'Пароли не совпадают' },
  )
  .refine(
    ({ oldPassword, confirmNewPassword, newPassword }) =>
      !(oldPassword || newPassword || confirmNewPassword)
      || (oldPassword && newPassword && confirmNewPassword),
    {
      path: ['common'],
      message: 'All passwords must comply with validation rules',
    },
  );
