import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
  CircularProgress,
} from '@mui/material';
import { useEffect, useRef } from 'react';
import { videosApi } from '../../../app/redux/videos/videosApi';
import { regions } from '../../../shared/constants/videosRegions';
import {
  selectVideosList,
  selectVideosPageToken,
  selectVideosRegionCode,
} from '../../../app/redux/videos/videosSelectors';
import { useAppDispatch, useAppSelector } from '../../../app/redux/store';
import {
  addVideos,
  setPageToken,
  setVideos,
  setRegionCode,
} from '../../../app/redux/videos/videosSlice';

import '../styles/videosView.scss';
import VideoCard from '../../../components/videos/video-card';
import { useObserver } from '../../../shared/hooks/useObserver';

function VideosView() {
  const regionCode = useAppSelector(selectVideosRegionCode);
  const pageToken = useAppSelector(selectVideosPageToken);
  const videosList = useAppSelector(selectVideosList);
  const loader = useRef<HTMLSpanElement | null>(null);
  const dispatch = useAppDispatch();
  const [fetchVideos] = videosApi.useLazyGetPopularVideosByRegionQuery();
  const { observe, unobserve, inView } = useObserver({
    threshold: 0.1,
  });

  useEffect(() => {
    if (inView) {
      fetchVideos({ regionCode, pageToken }).then((response) => {
        dispatch(addVideos(response.data?.items || []));
        dispatch(setPageToken(response.data?.nextPageToken || ''));
      });
    }
  }, [inView]);

  useEffect(() => {
    fetchVideos({ regionCode }).then((response) => {
      dispatch(setVideos(response.data?.items || []));
      dispatch(setPageToken(response.data?.nextPageToken || ''));
      observe(loader.current as HTMLSpanElement);
    });
    return () => {
      unobserve();
    };
  }, [regionCode]);
  const changeRegionHandle = (e: SelectChangeEvent) => {
    dispatch(setRegionCode(e.target.value));
  };
  return (
    <div className="videos-view">
      <Typography variant="h1" fontSize="3rem" marginBottom="2rem">
        Популярные видосики
      </Typography>
      <FormControl fullWidth sx={{ maxWidth: '300px' }}>
        <InputLabel sx={{ color: '#fff' }}>Регион</InputLabel>
        <Select
          size="small"
          value={regionCode}
          sx={{
            color: '#fff',
            '& svg': {
              color: '#fff',
            },
            '& fieldset': {
              borderColor: '#fff !important',
              outline: 'none !important',
              '&:hover': {
                borderColor: 'rgba(255,255,255, .8) !important',
              },
            },
          }}
          label="Регион"
          onChange={changeRegionHandle}
        >
          {regions.map((region) => (
            <MenuItem key={region.value} value={region.value}>
              {region.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <section className="videos-view__grid">
        {videosList.map((video) => (
          <VideoCard key={video.id} video={video} />
        ))}
      </section>
      <div className="videos-view__loader">
        <CircularProgress
          ref={loader}
          sx={{ color: '#fff', margin: '1rem 0' }}
        />
      </div>
    </div>
  );
}

export default VideosView;
