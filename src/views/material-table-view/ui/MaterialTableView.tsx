import React, { ChangeEvent, useMemo, useState } from 'react';
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from '@mui/material';
import { IDeveloper, IDeveloperFilters } from '../../../shared/types/developer';
import {
  selectLevelItems,
  selectSpecialityItems,
} from '../../../shared/constants/developers';

interface ITableDeveloper extends IDeveloper {
  age: number;
}

interface ITableDeveloperFilters extends IDeveloperFilters {
  age: string;
}

const developers: ITableDeveloper[] = [
  {
    fullName: 'Трофимов Роман Егорович',
    level: 'Senior',
    speciality: 'Backend',
    age: 19,
  },
  {
    fullName: 'Короткова Полина Николаевна',
    speciality: 'Frontend',
    level: 'Middle',
    age: 21,
  },
  {
    fullName: 'Маслов Ибрагим Платонович',
    speciality: 'Frontend',
    level: 'Junior',
    age: 37,
  },
  {
    fullName: 'Астахова Майя Макаровна',
    speciality: 'DevOps',
    level: 'Junior',
    age: 29,
  },
  {
    fullName: 'Семенова Александра Фёдоровна',
    speciality: 'Frontend',
    level: 'Senior',
    age: 28,
  },
  {
    fullName: 'Кудряшова Злата Фёдоровна',
    speciality: 'Backend',
    level: 'Middle',
    age: 27,
  },
  {
    fullName: 'Мешков Давид Николаевич',
    speciality: 'Backend',
    level: 'Junior',
    age: 26,
  },
  {
    fullName: 'Михайлова Юлия Глебовна',
    speciality: 'DevOps',
    level: 'Senior',
    age: 22,
  },
  {
    fullName: 'Рудаков Роман Андреевич',
    speciality: 'DevOps',
    level: 'Middle',
    age: 35,
  },
  {
    fullName: 'Никитина Виктория Максимовна',
    speciality: 'Backend',
    level: 'Junior',
    age: 23,
  },
  {
    fullName: 'Федотов Иван Эмильевич',
    speciality: 'Frontend',
    level: 'Junior',
    age: 24,
  },
  {
    fullName: 'Павлова Екатерина Дмитриевна',
    speciality: 'Frontend',
    level: 'Middle',
    age: 25,
  },
  {
    fullName: 'Фомин Артём Германович',
    speciality: 'Backend',
    level: 'Senior',
    age: 30,
  },
  {
    fullName: 'Попов Евгений Максимович',
    speciality: 'Backend',
    level: 'Junior',
    age: 27,
  },
  {
    fullName: 'Горбунова Анастасия Александровна',
    speciality: 'Frontend',
    level: 'Junior',
    age: 20,
  },
  {
    fullName: 'Комарова Милана Матвеевна',
    speciality: 'DevOps',
    level: 'Junior',
    age: 37,
  },
  {
    fullName: 'Сафонова Маргарита Яковлевна',
    speciality: 'Backend',
    level: 'Middle',
    age: 26,
  },
  {
    fullName: 'Баженова Ника Глебовна',
    speciality: 'Frontend',
    level: 'Middle',
    age: 46,
  },
  {
    fullName: 'Гончаров Владимир Георгиевич',
    speciality: 'DevOps',
    level: 'Senior',
    age: 15,
  },
  {
    fullName: 'Данилов Роман Эминович',
    speciality: 'Backend',
    level: 'Senior',
    age: 0,
  },
];

function MaterialTableView() {
  const [tableDevelopers] = useState(developers);
  const [filters, setFilters] = useState<ITableDeveloperFilters>({
    fullName: '',
    level: '',
    speciality: '',
    age: '',
  });

  const setDeveloperFilter = (
    filterName: keyof ITableDeveloperFilters,
    filterValue: ITableDeveloperFilters[keyof ITableDeveloperFilters],
  ) => {
    setFilters((oldFilters) => ({ ...oldFilters, [filterName]: filterValue }));
  };

  const filteredTableDevelopers = useMemo(
    () =>
      Object.entries(filters).reduce(
        (acc, [filterField, filterValue]) =>
          acc.filter((developer) =>
            developer[filterField as keyof ITableDeveloperFilters]
              .toString()
              .toLowerCase()
              .includes(filterValue.toLowerCase()),
          ),
        tableDevelopers,
      ),
    [tableDevelopers, filters],
  );

  const fullNameFilterHandle = (e: ChangeEvent<HTMLInputElement>) => {
    setDeveloperFilter('fullName', e.target.value);
  };

  const ageFilterHandle = (e: ChangeEvent<HTMLInputElement>) => {
    setDeveloperFilter('age', e.target.value);
  };

  const levelFilterHandle = (e: SelectChangeEvent) => {
    setDeveloperFilter('level', e.target.value);
  };

  const specialityFilterHandle = (e: SelectChangeEvent) => {
    setDeveloperFilter('speciality', e.target.value);
  };

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          gap: '1rem',
          marginBottom: '1rem',
          padding: '1rem',
        }}
        component={Paper}
      >
        <TextField
          sx={{ flex: '0 1 25%' }}
          label="Full name"
          value={filters.fullName}
          size="small"
          onChange={fullNameFilterHandle}
        />
        <TextField
          sx={{ flex: '0 1 25%' }}
          label="Age"
          value={filters.age}
          size="small"
          onChange={ageFilterHandle}
        />
        <FormControl sx={{ flex: '0 1 25%' }}>
          <InputLabel size="small">Level</InputLabel>
          <Select
            size="small"
            value={filters.level}
            label="Level"
            onChange={levelFilterHandle}
          >
            {selectLevelItems.map((level) => (
              <MenuItem key={level.value} value={level.value}>
                {level.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl sx={{ flex: '0 1 25%' }}>
          <InputLabel size="small">Speciality</InputLabel>
          <Select
            size="small"
            value={filters.speciality}
            label="Speciality"
            onChange={specialityFilterHandle}
          >
            {selectSpecialityItems.map((speciality) => (
              <MenuItem key={speciality.value} value={speciality.value}>
                {speciality.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell sx={{ fontSize: '1rem' }}>Full name</TableCell>
              <TableCell sx={{ fontSize: '1rem' }} align="center">
                Age
              </TableCell>
              <TableCell sx={{ fontSize: '1rem' }} align="center">
                Level
              </TableCell>
              <TableCell sx={{ fontSize: '1rem' }} align="center">
                Speciality
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filteredTableDevelopers.map((dev) => (
              <TableRow key={dev.fullName}>
                <TableCell>{dev.fullName}</TableCell>
                <TableCell align="center">{dev.age}</TableCell>
                <TableCell align="center">{dev.level}</TableCell>
                <TableCell align="center">{dev.speciality}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default MaterialTableView;
