import { z } from 'zod';

export const schema = z.object({
  person: z.union([
    z.literal('individual', {
      errorMap: () => ({ message: 'Поле обязательно' }),
    }),
    z.literal('company', { errorMap: () => ({ message: 'Поле обязательно' }) }),
  ]),
  phone: z
    .string({ required_error: 'Поле обязательно' })
    .min(1, 'Поле не должно быть пустым'),
  comment: z
    .string()
    .max(500, 'Максимальное кол-во символов - 500')
    .min(1, 'Поле не должно быть пустым'),
  reason: z.union([
    z.literal('quality', { errorMap: () => ({ message: 'Поле обязательно' }) }),
    z.literal('longDelivery', {
      errorMap: () => ({ message: 'Поле обязательно' }),
    }),
    z.literal('qualityServices', {
      errorMap: () => ({ message: 'Поле обязательно' }),
    }),
  ]),
  date: z.date(),
  accept: z.boolean().refine((val) => val, 'Поле обязательно'),
});
