import {
  TextField,
  Paper,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Button,
  Checkbox,
  FormControlLabel,
  FormHelperText,
  Radio,
  RadioGroup,
  Typography,
} from '@mui/material';
import InputMask from 'react-input-mask';
import { useForm, Controller } from 'react-hook-form';
import { z } from 'zod';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { zodResolver } from '@hookform/resolvers/zod';
import { schema } from '../validations/schema';
import '../styles/materialFormView.scss';

export const reasons = [
  { label: 'Не устраивает качество товара', value: 'quality' },
  { label: 'Долгая доставка', value: 'longDelivery' },
  { label: 'Не устраивает качество услуг', value: 'qualityServices' },
];

function MaterialFormView() {
  const {
    handleSubmit,
    register,
    formState: { errors },
    control,
    reset,
  } = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: {
      person: 'individual',
      accept: false,
      comment: '',
      date: new Date(),
      phone: '',
    },
  });

  const onSubmit = handleSubmit(() => {
    reset();
  });
  return (
    <Paper
      sx={{
        padding: '1rem',
        display: 'flex',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <form className="form" onSubmit={onSubmit}>
        <Typography variant="h4" textAlign="center">
          Оставьте жалобу
        </Typography>
        <FormControl error={!!errors.person?.message}>
          <Controller
            name="person"
            control={control}
            render={({ field }) => (
              <RadioGroup {...field}>
                <FormControlLabel
                  value="individual"
                  control={<Radio />}
                  label="Физ. лицо"
                />
                <FormControlLabel
                  value="company"
                  control={<Radio />}
                  label="Юр. лицо"
                />
              </RadioGroup>
            )}
          />
          {!!errors.person?.message && (
            <FormHelperText>{errors.person?.message}</FormHelperText>
          )}
        </FormControl>
        <Controller
          name="phone"
          control={control}
          render={({ field }) => (
            <InputMask {...field} mask="+7 (999) 999-99-99">
              <TextField
                {...field}
                label="Ваш номер телефона"
                size="small"
                fullWidth
                type="text"
                error={!!errors.phone?.message}
                helperText={errors.phone?.message}
              />
            </InputMask>
          )}
        />
        <TextField
          {...register('comment')}
          label="Комментарий"
          size="small"
          fullWidth
          multiline
          rows={5}
          error={!!errors.comment?.message}
          helperText={errors.comment?.message}
        />
        <FormControl error={!!errors.reason?.message}>
          <InputLabel size="small">Что не понравилось?</InputLabel>
          <Select
            defaultValue="quality"
            size="small"
            label="Что не понравилось?"
            {...register('reason')}
          >
            {reasons.map((reason) => (
              <MenuItem key={reason.value} value={reason.value}>
                {reason.label}
              </MenuItem>
            ))}
          </Select>
          {!!errors.reason?.message && (
            <FormHelperText>{errors.reason?.message}</FormHelperText>
          )}
        </FormControl>
        <FormControl error={!!errors.date?.message}>
          <Controller
            name="date"
            control={control}
            render={({ field: { onChange, ...rest } }) => (
              <DatePicker
                label="Введите дату (ну просто чтоб было, что, жалко что ли)"
                onChange={(event) => onChange(event)}
                slotProps={{
                  textField: {
                    size: 'small',
                  },
                }}
                {...rest}
              />
            )}
          />
          {!!errors.date?.message && (
            <FormHelperText>{errors.date?.message}</FormHelperText>
          )}
        </FormControl>
        <FormControl error={!!errors.accept?.message}>
          <FormControlLabel
            control={<Checkbox size="small" {...register('accept')} />}
            label="Принимаю политику обработки персональных данных"
          />
          {!!errors.accept?.message && (
            <FormHelperText>{errors.accept?.message}</FormHelperText>
          )}
        </FormControl>
        <Button type="submit" size="small" variant="contained">
          Отправить
        </Button>
      </form>
    </Paper>
  );
}

export default MaterialFormView;
