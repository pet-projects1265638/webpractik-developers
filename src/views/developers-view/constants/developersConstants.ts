import { IDeveloper } from '../../../shared/types/developer';

export const developersConstant: IDeveloper[] = [
  {
    fullName: 'Трофимов Роман Егорович',
    level: 'Senior',
    speciality: 'Backend',
  },
  {
    fullName: 'Короткова Полина Николаевна',
    speciality: 'Frontend',
    level: 'Middle',
  },
  {
    fullName: 'Маслов Ибрагим Платонович',
    speciality: 'Frontend',
    level: 'Junior',
  },
  {
    fullName: 'Астахова Майя Макаровна',
    speciality: 'DevOps',
    level: 'Junior',
  },
  {
    fullName: 'Семенова Александра Фёдоровна',
    speciality: 'Frontend',
    level: 'Senior',
  },
  {
    fullName: 'Кудряшова Злата Фёдоровна',
    speciality: 'Backend',
    level: 'Middle',
  },
  {
    fullName: 'Мешков Давид Николаевич',
    speciality: 'Backend',
    level: 'Junior',
  },
  {
    fullName: 'Михайлова Юлия Глебовна',
    speciality: 'DevOps',
    level: 'Senior',
  },
  {
    fullName: 'Рудаков Роман Андреевич',
    speciality: 'DevOps',
    level: 'Middle',
  },
  {
    fullName: 'Никитина Виктория Максимовна',
    speciality: 'Backend',
    level: 'Junior',
  },
  {
    fullName: 'Федотов Иван Эмильевич',
    speciality: 'Frontend',
    level: 'Junior',
  },
  {
    fullName: 'Павлова Екатерина Дмитриевна',
    speciality: 'Frontend',
    level: 'Middle',
  },
  {
    fullName: 'Фомин Артём Германович',
    speciality: 'Backend',
    level: 'Senior',
  },
  {
    fullName: 'Попов Евгений Максимович',
    speciality: 'Backend',
    level: 'Junior',
  },
  {
    fullName: 'Горбунова Анастасия Александровна',
    speciality: 'Frontend',
    level: 'Junior',
  },
  {
    fullName: 'Комарова Милана Матвеевна',
    speciality: 'DevOps',
    level: 'Junior',
  },
  {
    fullName: 'Сафонова Маргарита Яковлевна',
    speciality: 'Backend',
    level: 'Middle',
  },
  {
    fullName: 'Баженова Ника Глебовна',
    speciality: 'Frontend',
    level: 'Middle',
  },
  {
    fullName: 'Гончаров Владимир Георгиевич',
    speciality: 'DevOps',
    level: 'Senior',
  },
  {
    fullName: 'Данилов Роман Эминович',
    speciality: 'Backend',
    level: 'Senior',
  },
];
