import { useMemo, useState } from 'react';
import { IDeveloper, IDeveloperFilters } from '../../../shared/types/developer';
import { developersConstant } from '../constants/developersConstants';
import DeveloperCard from '../../../components/developers/developer-card';
import '../styles/developersView.scss';
import DeveloperFilters from '../../../components/developers/developer-filters';
import { filterDevelopersByField } from '../services/developersViewService';
import useResize from '../../../shared/hooks/useResize';
import DeveloperFormCreate from '../../../components/developers/developer-form-create';
import { DevelopersContext } from '../../../shared/constants/developers';

function DevelopersView() {
  const [developersList, setDevelopersList] = useState<IDeveloper[]>(developersConstant);
  const [filters, setFilters] = useState<IDeveloperFilters>({
    fullName: '',
    level: '',
    speciality: '',
  });
  const [toggleCreateForm, setToggleCreateForm] = useState(false);
  const { currentWindowSize } = useResize();

  console.log(currentWindowSize);

  const createDeveloper = (developer: IDeveloper) => {
    setDevelopersList((developers) => [...developers, developer]);
  };

  const deleteDeveloper = (developerIndex: number) => {
    setDevelopersList((developers) =>
      developers.filter((_, index) => index !== developerIndex),
    );
  };

  const setDeveloperFilter = (
    filterName: keyof IDeveloperFilters,
    filterValue: IDeveloperFilters[keyof IDeveloperFilters],
  ) => {
    setFilters((oldFilters) => ({ ...oldFilters, [filterName]: filterValue }));
  };
  const toggleDeveloperFormHandle = (flag = true) => {
    setToggleCreateForm(flag);
  };
  const submitDeveloperFormHandle = (developer: IDeveloper) => {
    createDeveloper(developer);
    toggleDeveloperFormHandle(false);
  };

  const filteredDevelopersList = useMemo(
    () =>
      Object.entries(filters).reduce(
        (acc, [filterField, filterValue]) =>
          filterDevelopersByField(
            acc,
            filterField as keyof IDeveloper,
            filterValue,
          ),
        developersList,
      ),
    [filters, developersList],
  );

  const context = useMemo(
    () => ({
      developersList,
      filters,
      createDeveloper,
      deleteDeveloper,
      setDeveloperFilter,
    }),
    [],
  );

  return (
    <DevelopersContext.Provider value={context}>
      <section className="developers-view">
        <div className="developers-view__actions">
          <div className="developers-view__filters">
            <DeveloperFilters />
          </div>
          <div className="developers-view__create">
            {toggleCreateForm && (
              <DeveloperFormCreate
                onChangeOpen={toggleDeveloperFormHandle}
                onSubmit={submitDeveloperFormHandle}
              />
            )}
            <button
              type="button"
              className="developers-view__create-btn"
              onClick={() => toggleDeveloperFormHandle()}
            >
              Create
            </button>
          </div>
        </div>
        <div className="developers-view__list">
          {filteredDevelopersList.map((developer, index) => (
            <DeveloperCard
              key={developer.fullName}
              developer={developer}
              developerIndex={index}
            />
          ))}
        </div>
      </section>
    </DevelopersContext.Provider>
  );
}

export default DevelopersView;
