import { IDeveloper } from '../../../shared/types/developer';

export function filterDevelopersByField(
  developers: IDeveloper[],
  fieldName: keyof IDeveloper,
  fieldValue: string,
) {
  if (!fieldValue) return developers;
  return developers.filter((developer) =>
    developer[fieldName].toLowerCase().includes(fieldValue.toLowerCase()),
  );
}
