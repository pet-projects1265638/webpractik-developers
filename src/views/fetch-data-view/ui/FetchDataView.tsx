import React from 'react';
import useFetch from '../../../shared/hooks/useFetch';
import withMessage from '../../../shared/HOCs/withMessage';
import AppButtonMessage, {
  IButtonMessage,
} from '../../../components/app/app-button-message';

type FakeApiResponse = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

function FetchDataView() {
  const { data, loading, error } = useFetch<FakeApiResponse[]>({
    url: 'https://jsonplaceholder.typicode.com/posts',
  });
  const WithMessageButton = withMessage<IButtonMessage>(AppButtonMessage);
  return (
    <div>
      <div style={{ margin: '2rem 0' }}>
        <WithMessageButton />
      </div>
      {loading && <span>Loading...</span>}
      {error && (
        <span>
          Error:
          {error}
        </span>
      )}
      {data?.map((item) => (
        <div key={item.id} style={{ marginBottom: '2rem' }}>
          <span>
            ID:
            {item.id}
          </span>
          <h1>{item.title}</h1>
          <p>{item.body}</p>
          <span>
            User ID:
            {item.userId}
          </span>
        </div>
      ))}
    </div>
  );
}

export default FetchDataView;
